#!/bin/bash
echo "Kill wine apps"
ps aux | grep "\.exe" | grep -v grep | awk '{print $2}' | xargs kill -9 &>/dev/null
echo "Kill wine"
killall winedevice.exe &>/dev/null
killall wineserver &>/dev/null
killall winedbg
