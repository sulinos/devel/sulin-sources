# Sulin sources
Additional SulinOS sources

| Source          | Description                   | Language   |
| --------------- | ----------------------------- | ---------- |
| misc            | troll and unused stuff :D     | Unknown    |
| mklinux         | linux kernel builder          | Bash       |
| txt2html.c      | text to html generator        | C          |
| backlight.initd | openrc backlight service      | openrc-run |
| docugen.py      | documentation generator       | python3    |
| oto.py          | Amele botu (Tuğrul anladı :D) | python3    |
| xmlvalidate.py  | Xml validator                 | python3    |
| libsulin.so     | Empty library (for linking)   | C          |
