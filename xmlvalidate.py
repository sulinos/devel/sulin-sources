#!/usr/bin/bash
import sys
xmlfile=sys.argv[1]

class xmlverify:

   def __init__(self,xml=""):
       self.xmldata=xml

   def validate(self,xml=None):
       if xml:
           self.xmldata=xml
       ch=self.validate_chars() 
       tg=self.validate_tags()
       return tg and ch

   def validate_chars(self):
        line="\n"
        e=None
        f=0
        error=False
        x=0

        bracked=0
        skipchar=False
        for line in self.xmldata.split("\n"):
            x=x+1
            y=0
            while y<len(line):
                c=line[y]
                y=y+1
                if skipchar:
                    skipchar=False
                    c=" "

                if c == '\\':
                    skipchar=True


                if (c == '\"' or c== '\'') and self.bracked != 0:
                    if e:
                        e=None
                    else:
                        e=c
                        f="{}:{}".format(x,y)
                if not e == c:
                    
                    if c == '<':
                        bracked=bracked+1
                    elif c == '>':
                        bracked=bracked-1
                    if bracked<0:
                        error=True
                        bracked=0
                        print("Error on {}:{} '>' unexcepted".format(self.current_line,line.index(c)))
 
        if e:
            error=True
            print("Error on {} missing".format(f),e)
        return error

   def validate_tags(self):
        error=False
        tmp=self.xmldata
        tags=[]
        tmp=tmp.strip()
        tmp=tmp.replace("\t","")
        tmp=tmp.replace("\n","")
        tmp=tmp.split(">")
        for tag in tmp:
            if len(tag) > 1 and '<' in tag:
                l=""
                if tag[len(tag)-1] == "/" :
                    l="/"
                tag=tag[tag.index('<')+1:]
                if " " in tag:
                    tag=tag.split(" ")[0]
                tag=tag+l
                tags.append(tag)

        num=0
        while num<len(tags):
            tag=tags[num]
            num=num+1
            if tag[0] != "/" and tag[len(tag)-1] != "/" and tag[0] != "?":
                num1=num
                found=False
                while num1<len(tags):
                    tag1=tags[num1]
                    num1=num1+1
                    if tag1[0] == "/" and tag1 == "/"+tag:
                        found=True
                if not found:
                    error=True
                    print("Error missing: </"+tag+">")
        num=len(tags)-1
        while num>0:
            tag=tags[num]
            num=num-1
            if tag[0] == "/":
                num1=num
                found=False
                while num1>0:
                    tag1=tags[num1]
                    num1=num1-1
                    if tag == "/"+tag1:
                        found=True
                if not found:
                    error=True
                    print("Error missing: <"+tag[1:]+">")
        return error

xmlverify(open(xmlfile,"r").read()).validate()
