DESTDIR=/
LIBDIR=/lib
build:
	$(CC) -o txt2html txt2html.c
	touch empty.c && $(CC) empty.c -o libsulin.so -shared && rm -f empty.c

install:
	mkdir -p $(DESTDIR)/bin $(DESTDIR)/etc/init.d || true
	install mklinux $(DESTDIR)/bin/
	install docugen.py $(DESTDIR)/bin/docugen
	install oto.py $(DESTDIR)/bin/oto
	install txt2html $(DESTDIR)/bin/txt2html
	install xmlvalidate.py $(DESTDIR)/bin/xmlvalidate
	install destroy-winzort.sh $(DESTDIR)/bin/destroy-winzort
	install backlight.initd $(DESTDIR)/etc/init.d/backlight
	install libsulin.so $(DESTDIR)/$(LIBDIR)/libsulin.so
	install pkgconfig-sulin.pc $(DESTDIR)/usr/share/pkgconfig/sulin.pc
clean:
	rm -f txt2html libsulin.so
